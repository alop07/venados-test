import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class VenadosService {

    private url = 'api/';
    constructor(
        private http: HttpClient
    ) {}

    public games(): Observable<any> {
        const headers = new HttpHeaders({'Accept': 'application/json'});
        return this.http.get(this.url + 'games', {headers});
    }

    public statistics(): Observable<any> {
        const headers = new HttpHeaders({'Accept': 'application/json'});
        return this.http.get(this.url + 'statistics', {headers});
    }

    public players(): Observable<any> {
        const headers = new HttpHeaders({'Accept': 'application/json'});
        return this.http.get(this.url + 'players', {headers});
    }
}
