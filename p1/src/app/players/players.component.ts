import { Component, OnInit } from '@angular/core';

import { VenadosService } from '../venados.service';

import { Player } from '../models/player.model';

import * as moment from 'moment';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  public player: Player = new Player();

  public data = new Array<Player>();

  constructor(
    private venadosSrv: VenadosService
  ) {
    this.players();
  }

  ngOnInit() { }

  players() {
    this.venadosSrv.players().subscribe(
      response => {

      for (const item of response.data.team.forwards) {
        this.add(item);
      }

      for (const item of response.data.team.centers) {
        this.add(item);
      }

      for (const item of response.data.team.defenses) {
        this.add(item);
      }

      for (const item of response.data.team.goalkeepers) {
        this.add(item);
      }

      for (const item of response.data.team.coaches) {
        this.add(item);
      }

    }
  );
  }

  add(item: any) {
    const player = new Player();
    player.addFromResponse(item);
    this.data.push(player);
  }

  setPlayer(item: Player) {
    this.player = item;
  }

  formatBirthday(date: string) {
    return moment(date.substring(0, 10)).format('DD/MM/YYYY');

  }

}
