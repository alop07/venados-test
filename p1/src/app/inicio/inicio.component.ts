import { Component, OnInit } from '@angular/core';

import { VenadosService } from '../venados.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  public data = [];
  public islocal = true;

  constructor(
    private venadosSrv: VenadosService
  ) {
    this.games();
  }

  games() {
    this.venadosSrv.games().subscribe(
      response => {

        for (const game of response.data.games) {

          const d = game.datetime.substring(5, 7);

          const month = this.getNameMonth(d);

          if (this.getGamesByMonth(month) === undefined) {

            this.data.push({ name : month , items: new Array<any>() });
          }

          const item =  this.getGamesByMonth(month);
          item.items.push(game);

        }

      }
  );
  }

  getGamesByMonth(month: string) {

    for (const item of this.data) {
      if (item.name === month) {
        return item;
      }
    }

    return undefined;
  }

  getNameMonth(m: string) {
      let month: string;

      switch (m) {
        case '01':
          month = 'Enero';
          break;
        case '02':
          month = 'Febrero';
          break;

        case '03':
          month = 'Marzo';
          break;

        case '04':
          month = 'Abril';
          break;

        case '05':
          month = 'Mayo';
          break;

        case '06':
          month = 'Junio';
          break;

        case '07':
          month = 'Julio';
          break;

        case '08':
          month = 'Agosto';
          break;

        case '09':
          month = 'Septiembre';
          break;

        case '10':
          month = 'Octubre';
          break;

        case '11':
          month = 'Noviembre';
          break;

        case '12':
          month = 'Diciembre';
          break;
      }

      return month;
  }


  getDay(d: string) {

    const date = new Date(d);

    return date.getUTCDate();
  }

  getNameDaySpanish(d: string) {

    const date = new Date(d.substring(0, 10) + 'T00:00:01+00:00');
    const days: Array<string> = ['LUN', 'MAR', 'MIÉR', 'JUE', 'VIER', 'SÁB', 'DOM'];
    return days[date.getDay()];
  }


  ngOnInit() {
  }

}
