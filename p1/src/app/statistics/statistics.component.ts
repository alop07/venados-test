import { Component, OnInit } from '@angular/core';

import { VenadosService } from '../venados.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  public data = [];

  constructor(
    private venadosSrv: VenadosService

  ) {
    this.statistics();
   }

  ngOnInit() {
  }

  statistics() {
    this.venadosSrv.statistics().subscribe(
      response => {

       this.data = response.data.statistics;
      }
  );
  }

}
