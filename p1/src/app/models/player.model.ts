export class Player {

    public name: string = undefined;
    public firstSurname: string = undefined;
    public secondSurname: string = undefined;
    public birthday: string = undefined;
    public birthPlace: string = undefined;
    public weight: number = undefined;
    public height: number = undefined;
    public position: string = undefined;
    public number: number = undefined;
    public positionShort: string = undefined;
    public lastTeam: string = undefined;
    public image: string = undefined;
    public fullName: string = undefined;

    constructor() {
        this.birthday = '';
    }

    public addFromResponse(dataObject: any): void {
        if (dataObject !== undefined && dataObject !== null) {
            this.name             = dataObject.name;
            this.firstSurname    = dataObject.first_surname;
            this.secondSurname    = dataObject.second_surname;
            this.birthday           = dataObject.birthday;
            this.birthPlace          = dataObject.birth_place;
            this.weight            = dataObject.weight;
            this.height             = dataObject.height;
            this.position             = dataObject.position;
            this.number             = dataObject.number;
            this.positionShort             = dataObject.position_short;
            this.lastTeam             = dataObject.last_team;
            this.image             = dataObject.image;

            this.fullName = this.name + ' ' + this.firstSurname + ' ' + this.secondSurname;
        }
    }

}
