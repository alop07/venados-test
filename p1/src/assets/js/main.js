
window.onscroll = function() {
    let des_act = window.pageYOffset;

    if (document.getElementById('content_logo_inicio') !== null && document.getElementById('content_tab_inicio') !== null && document.getElementById('content_content_inicio') !== null) {

        if ( des_act === 0) {
            document.getElementById('content_logo_inicio').style.display = 'block';
            document.getElementById('content_tab_inicio').classList.remove('uk-position-fixed','uk-position-top');
            document.getElementById('content_content_inicio').classList.remove('uk-margin-large-top');

        } else {
            this.document.getElementById('content_logo_inicio').style.display = 'none';
            this.document.getElementById('content_tab_inicio').classList.add('uk-position-fixed', 'uk-position-top');
            this.document.getElementById('content_content_inicio').classList.add('uk-margin-large-top');

        }
    }

}